//
//  AlertManager.swift
//  Veridos
//
//  Created by Abdiel Soto on 12/31/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import SwiftSpinner

class Spinner: NSObject {
  
  class func show(_ title: String) {
    SwiftSpinner.show(delay: 0.0, title: title, animated: true)
  }
  
  class func show(_ title: String, waiting: Double, completion: @escaping () -> ()) {
    show(title)
    DispatchQueue.wait(seconds: waiting) {
      hide()
      completion()
    }
  }
  
  class func hide(){
    SwiftSpinner.hide()
  }
}


