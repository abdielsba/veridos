//
//  Alert.swift
//  Veridos
//
//  Created by Abdiel Soto on 1/3/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import UIKit

class Alert: UIView {

   func show(_ title: String) {
    let alertController = UIAlertController(title: "Veridos", message: title, preferredStyle: .alert)
    let action1 = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
      print("You've pressed default");
    }
    let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
      print("You've pressed cancel");
    }
    alertController.addAction(action1)
    alertController.addAction(action2)
    UIApplication.shared.topMostViewController()?.present(alertController, animated: true, completion: nil)
  }
}
