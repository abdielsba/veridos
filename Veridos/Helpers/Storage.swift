//
//  Storage.swift
//  Veridos
//
//  Created by Abdiel Soto on 2/11/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import UIKit
import Identy

class Storage {
  
  var userPhoto: UIImage?
  
  private(set) var IDInfo: IDInfo?
  
  private init() { }
  static let shared = Storage()
  
  func add(IDInfo: IDInfo) {
    self.IDInfo = IDInfo
  }
  
}
