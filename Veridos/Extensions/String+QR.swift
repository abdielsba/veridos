//
//  String+QR.swift
//  Veridos
//
//  Created by Abdiel Soto on 12/31/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

extension String {
  
  func IDData() -> IDInfo? {
    
    let qrResult = self.split(separator: "|")
    guard qrResult.count >= 24 else { return nil }
    
    let lastName = String(qrResult[0])
    let middleName = String(qrResult[1])
    let name = String(qrResult[2])
    let electorKey = String(qrResult[3])
    let curp = String(qrResult[4])
    let birthDay = String(qrResult[5])
    let date = String(qrResult[6])
    let gender = String(qrResult[7])
    let emisionYear = String(qrResult[8])
    let registerYear = String(qrResult[9])
    let state = String(qrResult[10])
    let municipality = String(qrResult[11])
    let section = String(qrResult[12])
    let locality = String(qrResult[13])
    let unknownId1 = String(qrResult[14])
    let unknownId2 = String(qrResult[15])
    let address1 = String(qrResult[16])
    let address2 = String(qrResult[17])
    let address3 = String(qrResult[18])
    let unknownId3 = String(qrResult[19])
    let fingerNumber1 = String(qrResult[20])
    let fingerData1 = String(qrResult[21])
    let fingerNumber2 = String(qrResult[22])
    let fingerData2 = String(qrResult[23])
    
    return IDInfo(lastName: lastName, middleName: middleName, name: name, electorKey: electorKey, curp: curp, birthDay: birthDay, date: date, gender: gender, emisionYear: emisionYear, registerYear: registerYear, state: state, municipality: municipality, section: section, locality: locality, unknownId1: unknownId1, unknownId2: unknownId2, address1: address1, address2: address2, address3: address3, unknownId3: unknownId3, fingerNumber1: fingerNumber1, fingerData1: fingerData1, fingerNumber2: fingerNumber2, fingerData2: fingerData2)
  }
  
}
