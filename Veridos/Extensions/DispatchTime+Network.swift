//
//  DispatchTime+Network.swift
//  Veridos
//
//  Created by Abdiel Soto on 1/7/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import Foundation

extension DispatchQueue {
  
  // Dummy method to simulate a network task
  static func wait(seconds: Double, completion: @escaping () -> ()) {
    let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: popTime) {
      completion()
    }
  }
}
