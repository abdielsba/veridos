//
//  Theme.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/24/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

struct Theme {
  
  static func changeNavigationBar() {
    UINavigationBar.appearance().barTintColor = UIColor(red: 34.0/255.0, green: 35.0/255.0, blue: 38.0/255.0, alpha: 1.0)
    UINavigationBar.appearance().tintColor = .white
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    UINavigationBar.appearance().isTranslucent = false
  }
 
  static let mainBackgroundColor = UIColor(red: 42.0/255.0, green: 43.0/255.0, blue: 47.0/255.0, alpha: 1.0)
}
