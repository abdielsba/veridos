//
//  Coordinator.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/30/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import Identy
import swiftScan
import DOT

class Coordinator {
  
  // MARK: - Properties
  let navigationViewController: UINavigationController?
  
  // MARK: - Initialization
  init(navigationViewController: UINavigationController?) {
    self.navigationViewController = navigationViewController
  }
  
  
  func showAlert(_ title: String, completion: (() -> Void)? = nil) {
    let alertController = UIAlertController(title: "Veridos", message: title, preferredStyle: .alert)
    let action1 = UIAlertAction(title: "Aceptar", style: .default) { (action:UIAlertAction) in
      completion?()
    }

    alertController.addAction(action1)
    navigationViewController?.present(alertController, animated: true, completion: nil)
  }
  
  func showAlertQR(_ title: String, trueHandler: @escaping () -> Void, falseHandler: @escaping () -> Void) {
    let alertController = UIAlertController(title: "Veridos", message: title, preferredStyle: .alert)
    let action1 = UIAlertAction(title: "Si", style: .default) { (action:UIAlertAction) in
      trueHandler()
    }
    
    let action2 = UIAlertAction(title: "No", style: .default) { (action:UIAlertAction) in
      falseHandler()
    }
    
    alertController.addAction(action1)
    alertController.addAction(action2)
    navigationViewController?.present(alertController, animated: true, completion: nil)
  }
  
  // MARK: - Public
  func showResults(_ results: ResultData) {
    let resultsViewController = ResultsViewController(results: results)
    navigationViewController?.pushViewController(resultsViewController, animated: true)
  }
  
  func showPhoto(title: String? = nil, _ image: UIImage? = nil,  IDInfo: IDInfo? = nil, buttonTitle: String? = nil, completion: @escaping () -> Void) {
    let imageFullViewController = ImageFullViewController(image: image, IDInfo: IDInfo)
    imageFullViewController.didButtonPressed = completion
    imageFullViewController.title = title
    imageFullViewController.buttonTitle = buttonTitle
    let navViewController = UINavigationController(rootViewController: imageFullViewController)
    navigationViewController?.present(navViewController, animated: true, completion: nil)
  }
  
}

// MARK: - Cameras
extension Coordinator {
  func showEnrollCamera(IDInfo: IDInfo? = nil, completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    guard let viewController = navigationViewController else { return }
    // Manager instance
    let identyManager = IdentyManager(IDInfo: IDInfo,viewController: viewController)
    // Enroll
    identyManager.enroll(completion: completion)
  }
  
  func showVerifyCamera(IDInfo: IDInfo? = nil, completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    guard let viewController = navigationViewController else { return }
    // Manager instance
    let identyManager = IdentyManager(IDInfo: IDInfo,viewController: viewController)
    // Enroll
    identyManager.verify(completion: completion)
  }
  
  
  func showFaceCamera(delegate: HomeViewController) {
    
    if UserDefaults.standard.array(forKey: "template") == nil {
      let controller = FaceCaptureController.create(requestCropImage: true, requestTemplate: true)
      controller.delegate = delegate
      navigationViewController?.pushViewController(controller, animated: true)
    } else {
      showFaceVerificationCamera(delegate: delegate)
    }
  }
  
  func showFaceVerificationCamera(delegate: HomeViewController) {
    if UserDefaults.standard.array(forKey: "template") == nil {
      showAlert("Firstly you need to capture photo of your face.")
    } else {
      let controller = DemoVerificationController.create()
      controller.delegate = delegate
      navigationViewController?.pushViewController(controller, animated: true)
    }
  }
  
  func showQRCamera(delegate: QQScanViewControllerDelegate? = nil ) {
    // Create controller
    let scanViewController = QQScanViewController()
    scanViewController.resultDelegate = delegate
    // Style
    var style = LBXScanViewStyle()
    style.animationImage = UIImage(named: "CodeScan.bundle/qrcode_scan_light_green")
    scanViewController.scanStyle = style
    // Push controller
    navigationViewController?.pushViewController(scanViewController, animated: true)
  }
}
