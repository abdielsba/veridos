//
//  HomeViewController+FaceCapture.swift
//  Veridos
//
//  Created by Abdiel Soto on 1/24/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import UIKit
import DOT

extension HomeViewController: DemoVerificationControllerDelegate {
  func demoVerificationController(_ controller: DemoVerificationController, verified: Bool) {
    
    guard verified else {
      coordinator.showAlert("La verificación del rostro no fue correcta, por favor intentelo de nuevo") { [weak self] in
        self?.coordinator.showResults([[.rostro: false],
                                       [.autentica: false],
                                       [.integra: false],
                                       [.valida: false]])
      }
      return
    }

    Spinner.show("Validando...", waiting: 3.0) {
      // 4. Show results
      self.coordinator.showResults([[.rostro: true],
                                    [.autentica: true],
                                    [.integra: true],
                                    [.valida: true]])
    }
  }
}

extension HomeViewController: FaceCaptureControllerDelegate {
  
  func faceCapture(_ controller: FaceCaptureController, stateChanged state: FaceCaptureState) {
    debugPrint(#function, state)
  }
  
  func faceCaptureCameraInitFailed(_ controller: FaceCaptureController) {
    debugPrint(#function)
  }
  
  func faceCaptureNoCameraPermission(_ controller: FaceCaptureController) {
    debugPrint(#function)
   
  }
  
  func faceCapture(_ controller: FaceCaptureController, didCapture faceCaptureImage: FaceCaptureImage) {
    debugPrint(#function)
    UserDefaults.standard.set(faceCaptureImage.template, forKey: "template")
    Spinner.show("Procesando...", waiting: 2.0)  {
      Storage.shared.userPhoto = faceCaptureImage.croppedImage
      // 4. Show results
      self.coordinator.showPhoto(title: "Foto descargada", faceCaptureImage.croppedImage, completion: { [weak self] in
        guard let self = self else { return }
        // 4. Show results
        self.coordinator.showFaceVerificationCamera(delegate: self)
      })
    }
  }
  
  func faceCaptureDidFailed(_ controller: FaceCaptureController) {
    debugPrint(#function)
    controller.navigationController?.popViewController(animated: true)
    coordinator.showAlert("Face Capture did failed")
  }
  
  func faceCaptureDidStartPhotoCaptureStep(_ controller: FaceCaptureController) {
    debugPrint(#function)
  }
  
  func faceCaptureWillAppear(_ controller: FaceCaptureController) {
    debugPrint(#function)
    controller.title = "Veridos"
    controller.navigationItem.backBarButtonItem?.title = " "
  }
  
  func faceCaptureDidLoad(_ controller: FaceCaptureController) {
    debugPrint(#function)
  }
  
  func faceCaptureWillDisappear(_ controller: FaceCaptureController) {
    debugPrint(#function)
  }
  
  func faceCaptureDidDisappear(_ controller: FaceCaptureController) {
    debugPrint(#function)
  }
  
  func faceCaptureDidAppear(_ controller: FaceCaptureController) {
    debugPrint(#function)
    controller.requestFaceCapture()
  }
}
