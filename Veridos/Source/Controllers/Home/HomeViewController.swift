//
//  HomeViewController.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/22/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import swiftScan
import Identy

class HomeViewController: UIViewController {
  
  // MARK: - IBOulets
  @IBOutlet weak var collectionView: UICollectionView!
  
  // MARK: - Properties
  let dataSource = MenuDataSource.data
  var optionSelected: MenuType = .none
  let viewModel = HomeViewModel()
  
  lazy var coordinator: Coordinator = {
    return Coordinator(navigationViewController: navigationController)
  }()
  @IBAction func cleanData(_ sender: Any) {
    coordinator.showAlert("Los datos de enrolamiento se han borrado correctamente")
    DOTManager.clearData()
    IdentyManager.clearData()
  }
  
  // MARK: - Life cycle
  override func viewDidLoad() {
    
    DOTManager.initialize()
    
    super.viewDidLoad()
    viewModel.bindState { [weak self] state in
      guard let self = self else { return }
      switch state {
      // Success
      case .successID(let IDInfo):
        switch self.optionSelected {
        case .visualizarIne:
          // Dismiss QR
          self.coordinator.navigationViewController?.popViewController(animated: true)
          // 2. Read fingers
          self.showFingerCamera(IDInfo: IDInfo) { [weak self] result in
            // Strong self
            guard let self = self else { return }
            switch result {
            case .success(let response):
              print("Handle error \(String(describing: response.responseDictionary))")
              
              Storage.shared.add(IDInfo: IDInfo)

              Spinner.show("Validando", waiting: 3.0) {
                // 4. Show results
                self.coordinator.showResults([[.huella: true],
                                              [.autentica: true],
                                              [.integra: true],
                                              [.valida: true]])
              }
            case .failure(let error):
              let errorCustom = error as ErrorResult
              self.coordinator.showAlert(errorCustom.localizedDescription) {
                // 4. Show results
                self.coordinator.showResults([[.huella: false],
                                              [.autentica: false],
                                              [.integra: false],
                                              [.valida: false]])
              }
            }
          }
          print("WSQ succesfully \(IDInfo)")
        case .verificarIne:
          // 2. Download image
          Spinner.show("Descargando información...", waiting: 2.0) {
            // Present face camera
            self.coordinator.showFaceCamera(delegate: self)
         
          }
        case .verificarOffline:
          // 2. Read fingers
          // Dismiss QR camera
          self.coordinator.navigationViewController?.popViewController(animated: true)
          self.showFingerCamera { [weak self] result in
            // Strong self
            guard let self = self else { return }
            switch result {
            case .success:
              Spinner.show("Validando...", waiting: 3.0) {
                self.coordinator.showResults([[.huella: true],
                                              [.autentica: true],
                                              [.integra: true],
                                              [.valida: true]])
              }
            case .failure(let error):
              self.coordinator.showAlert(error.localizedDescription) {
                self.coordinator.showResults([[.huella: false],
                                              [.autentica: false],
                                              [.integra: false],
                                              [.valida: false]])
              }
            }
          }
        case .validarExtranjero:
          // 2. Read fingers
          // Dismiss QR camera
          self.coordinator.navigationViewController?.popViewController(animated: true)
          self.showFingerCamera { [weak self] result in
            // Strong self
            guard let self = self else { return }
            switch result {
            case .success:
              Spinner.show("Validando...", waiting: 3.0) {
                self.coordinator.showResults([[.huella: true],
                                              [.autentica: true],
                                              [.integra: true],
                                              [.validaExtranjero: true]])
              }
            case .failure(let error):
              
              self.coordinator.showAlert(error.localizedDescription) {
                self.coordinator.showResults([[.huella: false],
                                              [.autentica: false],
                                              [.integra: false],
                                              [.validaExtranjero: false]])
              }
            }
          }
        default:
          break
        }
        
      case .failure:
        break
      default:
        break
      }
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  // MARK: - Private
  private func showFingerCamera(IDInfo: IDInfo? = nil, completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    // Check if the user has been enrolled
    guard IdentyManager.isEnrolled() else {
      Spinner.show("Enrolamiento de huellas...", waiting: 2.0) {
        // Show enrollment
        self.coordinator.showEnrollCamera(IDInfo: self.viewModel.IDInfo, completion: { result in
          switch result {
          case .success:
            self.showVerifyFingerCamera(completion: completion)
          case .failure:
            DispatchQueue.wait(seconds: 1.0, completion: {
              completion(result)
            })
          }
        })
      }
      return
    }
    showVerifyFingerCamera(completion: completion)
  }
  
  private func showVerifyFingerCamera(completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    Spinner.show("Verificación de huellas...", waiting: 2.0) {
      // Verify fingers
      self.coordinator.showVerifyCamera(IDInfo: self.viewModel.IDInfo, completion: { result in
        switch result {
        case .success:
          completion(result)
        case .failure:
          DispatchQueue.wait(seconds: 1.0, completion: {
            completion(result)
          })
        }
      })
    }
  }
}

// MARK: - Step #1 Menu selection
extension HomeViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let item = dataSource[indexPath.row]
    optionSelected = item.type
    // All menu options go to the QR camera
    switch item.type {
    case .visualizarIne,
         .verificarIne,
         .verificarOffline,
         .validarExtranjero:
      coordinator.showQRCamera(delegate: self)
    case .miIne:
      guard let IDInfo = Storage.shared.IDInfo else {
        coordinator.showAlert("Para visualizar tu INE primero virtualizarla, dirige al menú y selecciona \"Virtualizar INE\".")
        return
      }
      
      // 2. Download image
      Spinner.show("Descargando información...", waiting: 2.0) {
        // 4. Show results
        self.coordinator.showPhoto(title: "Credencial", Storage.shared.userPhoto, IDInfo: IDInfo, buttonTitle: "Continuar", completion: { [weak self] in
          // Strong self
          guard let self = self else { return }
          
          // 5. Show QR
          self.coordinator.showPhoto(title: "Código QR", UIImage(named: "qr"), completion: { [weak self] in
            // Strong self
            guard let self = self else { return }
            // Return to home
            self.coordinator.navigationViewController?.popToRootViewController(animated: true)
          })
        })
      }
      
    default:
      break
    }
  }
}

// MARK: - Step #2 QR Camera Result
extension HomeViewController: QQScanViewControllerDelegate {
  func handleCodeResult(arrayResult: [LBXScanResult]) {
    switch optionSelected {
    case .visualizarIne,
         .verificarIne,
         .verificarOffline,
         .miIne,
         .validarExtranjero:
      viewModel.getQRInfo(arrayResult)
    default:
      break
    }
  }
}

// MARK: - Face Camera Result
extension HomeViewController: FaceCameraViewControllerDelegate {
  
  func handleResult(result: Result<UIImage, ErrorResult>) {
    switch optionSelected {
    case .verificarIne:
      switch result {
      case .success(let image):
        print("Handle image taken")
	        Spinner.show("Validando...", waiting: 3.0) {
          // 4. Show results
            self.coordinator.showResults([[.rostro: true],
                                          [.autentica: true],
                                          [.integra: true],
                                          [.valida: true]])
        }
      case .failure(let error):
         self.coordinator.showResults([[.rostro: false],
                                       [.autentica: false],
                                       [.integra: false],
                                       [.valida: false]])
      }
    default:
      break
    }
  }
}
