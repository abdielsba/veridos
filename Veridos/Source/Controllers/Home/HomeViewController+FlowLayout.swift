//
//  HomeViewController+FlowLayout.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/24/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

extension HomeViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    // width 45%, height 17%
    return CGSize(width: view.frame.size.width * 0.46, height: view.frame.size.height * 0.242)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 12, bottom: 5, right: 12)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 5
  }
}
