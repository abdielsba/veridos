//
//  HomeViewController+DataSource.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/24/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

extension HomeViewController: UICollectionViewDataSource {

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let menuItem = dataSource[indexPath.row]    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
    cell.setup(item: menuItem)
    return cell
  }
}

