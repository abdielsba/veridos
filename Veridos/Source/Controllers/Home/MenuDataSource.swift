//
//  MenuDataSource.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/25/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

class MenuDataSource {
  
  // MARK: - Properties
  static let data = [
    MenuItem(type: .visualizarIne,     text: "VIRTUALIZAR INE ",         image: UIImage(named: "card1")),
    MenuItem(type: .verificarIne,      text: "VERIFICAR INE",            image: UIImage(named: "card2")),
    MenuItem(type: .verificarOffline,  text: "VERIFICAR OFFLINE",        image: UIImage(named: "card3")),
    MenuItem(type: .miIne,             text: "MI INE",                   image: UIImage(named: "card4")),
    MenuItem(type: .validarExtranjero, text: "VALIDAR EN EL EXTRANJERO", image: UIImage(named: "card5"))
  ]
}
