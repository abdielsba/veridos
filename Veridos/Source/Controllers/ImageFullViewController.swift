//
//  ImageFullViewController.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/30/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

class ImageFullViewController: UIViewController {
  
  // MARK: - IBOutlets
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var doneButton: UIButton!
  @IBOutlet weak var ineView: INEView!
  
  var buttonTitle: String?
  
  // MARK: - Public properties
  var didButtonPressed: (() -> ())?
  // MARK: - Private properties
  private let photImage: UIImage?
  private let IDInfo: IDInfo?

  // MARK: - Initialization
  init(image: UIImage? = nil, IDInfo: IDInfo? = nil) {
    self.photImage = image
    self.IDInfo = IDInfo
    super.init(nibName: "ImageFullViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let title = buttonTitle {
      doneButton.setTitle(title, for: .normal)
    }
    
    // Hide INE
    if IDInfo == nil {
      imageView.image = photImage
      ineView.isHidden = true
    } else {
      ineView.imsgeView.image = photImage
      ineView.setIDInfo(IDInfo)
    }
  }
  
  // MARK: - Actions
  @IBAction func donePressed(_ sender: Any) {
    self.dismiss(animated: true) {
      self.didButtonPressed?()
    }
  }
}
