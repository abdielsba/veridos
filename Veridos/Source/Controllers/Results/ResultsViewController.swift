//
//  ResultsViewController.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/22/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

enum ResultType: String {
  case huella = "Huella"
  case rostro = "Rostro"
  case autentica = "Autentica"
  case integra = "Integra"
  case valida = "Valida"
  case validaExtranjero = "Valida en el extranjero"
}

class ResultItem {
  let type: ResultType
  let text: String
  var isOn: Bool
  
  init(type: ResultType, text: String, isOn: Bool = false) {
    self.type = type
    self.text = text
    self.isOn = isOn
  }
}

typealias ResultData = [[ResultType: Bool]]

class ResultsViewController: UIViewController {

  // MARK: - IBOulet
  @IBOutlet weak var tableView: UITableView!
  
  // MARK: - Properties
  private var results: ResultData = []
  private var dataSource: [ResultItem] = []
  struct Constants {
    static let cellIdentifier = String(describing: ResultStatusTableViewCell.self)
  }
  
  // MARK: - Initialization
  init(results: ResultData) {
    self.results = results
    
    for result in results {
      for type in result.keys {
        let item = ResultItem(type: type, text: type.rawValue, isOn: result[type] ?? false)
        dataSource.append(item)
      }
    }
    
    super.init(nibName: "ResultsViewController", bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - View Lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.separatorStyle = .none
    tableView.register(UINib(nibName: Constants.cellIdentifier, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
    
    // Custom back button
    let backButtonItem = UIBarButtonItem(title: "Inicio", style: .plain, target: self, action: #selector(ResultsViewController.backToHome(sender:)))
    self.navigationItem.leftBarButtonItem = backButtonItem
  }
  
  @objc func backToHome(sender: UIBarButtonItem) {
    navigationController?.popToRootViewController(animated: true)
  }
}

extension ResultsViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let item = dataSource[indexPath.row]
    let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath) as! ResultStatusTableViewCell
    cell.setup(item: item)
    return cell
  }
}

extension ResultsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 92
  }
}
