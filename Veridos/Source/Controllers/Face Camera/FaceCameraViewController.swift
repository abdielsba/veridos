//
//  FaceCameraViewController.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/29/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import AVFoundation

protocol FaceCameraViewControllerDelegate: class {
  func handleResult(result: Result<UIImage, ErrorResult>)
}

class FaceCameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {
  
  // MARK: - Properties
  weak var delegate: FaceCameraViewControllerDelegate?
  
  // MARK: - Camera Properties
  var captureSesssion: AVCaptureSession!
  var cameraOutput: AVCapturePhotoOutput!
  var previewLayer: AVCaptureVideoPreviewLayer!
  
  // MARK:  IBOulets
  @IBOutlet weak var previewView: UIView!
  
  // MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    captureSesssion = AVCaptureSession()
    captureSesssion.sessionPreset = AVCaptureSession.Preset.hd1280x720
    // Capture
    let cameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
    guard let device = cameraDevice,
      let input = try? AVCaptureDeviceInput(device: device),
      captureSesssion.canAddInput(input) else {
      print("Error realted to AVCaptureDeviceInput")
      return
    }
    // Add capture input
    captureSesssion.addInput(input)
    
    // Add capture output
    cameraOutput = AVCapturePhotoOutput()
    if captureSesssion.canAddOutput(cameraOutput) {
      captureSesssion.addOutput(cameraOutput)
      previewLayer = AVCaptureVideoPreviewLayer(session: captureSesssion)
      previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
      previewLayer.frame = view.bounds
      previewView.layer.addSublayer(previewLayer)
      captureSesssion.startRunning()
    }
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  // MARK: - Actions Take picture button
  @IBAction func didPressTakePhoto(_ sender: UIButton) {
    let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
    cameraOutput.capturePhoto(with: settings, delegate: self)
  }
  
  func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
    // Check if there is any error in capturing
    guard error == nil else {
      delegate?.handleResult(result: .failure(.custom(string: "Fail to capture photo: \(String(describing: error))")))
      return
    }
    // Check if the pixel buffer could be converted to image data
    guard let imageData = photo.fileDataRepresentation() else {
      delegate?.handleResult(result: .failure(.custom(string: "Fail to convert pixel buffer")))
     return
    }
    
    // Check if UIImage could be initialized with image data
    guard let capturedImage = UIImage.init(data: imageData , scale: 1.0) else {
      delegate?.handleResult(result: .failure(.custom(string: "Fail to convert image data to UIImage")))
      return
    }
    
    // Get original image width/height
    let imgWidth = capturedImage.size.width
    let imgHeight = capturedImage.size.height
    // Get origin of cropped image
    let imgOrigin = CGPoint(x: (imgWidth - imgHeight)/2, y: (imgHeight - imgHeight)/2)
    // Get size of cropped iamge
    let imgSize = CGSize(width: imgHeight, height: imgHeight)
    
    // Check if image could be cropped successfully
    guard let imageRef = capturedImage.cgImage?.cropping(to: CGRect(origin: imgOrigin, size: imgSize)) else {
      print("Fail to crop image")
      return
    }
    
    // Convert cropped image ref to UIImage
    let  imageToSave = UIImage(cgImage: imageRef, scale: 1.0, orientation: .down)
    delegate?.handleResult(result: .success(imageToSave))

    // Stop video capturing session (Freeze preview)
    captureSesssion.stopRunning()
  }
  
  // This method you can use somewhere you need to know camera permission   state
  private func askPermission() {
    let cameraPermissionStatus =  AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
    
    switch cameraPermissionStatus {
    case .authorized:
      print("Already Authorized")
    case .denied:
      print("denied")
      let alert = UIAlertController(title: "Sorry :(" , message: "But  could you please grant permission for camera within device settings",  preferredStyle: .alert)
      let action = UIAlertAction(title: "Ok", style: .cancel,  handler: nil)
      alert.addAction(action)
      present(alert, animated: true, completion: nil)
      
    case .restricted:
      print("restricted")
    default:
      AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {
        [weak self]
        (granted :Bool) -> Void in
        
        if granted == true {
          // User granted
          print("User granted")
          DispatchQueue.main.async(){
            //Do smth that you need in main thread
          }
        }
        else {
          // User Rejected
          print("User Rejected")
          
          DispatchQueue.main.async(){
            let alert = UIAlertController(title: "WHY?" , message:  "Camera it is the main feature of our application", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self?.present(alert, animated: true, completion: nil)
          }
        }
      });
    }
  }
}


