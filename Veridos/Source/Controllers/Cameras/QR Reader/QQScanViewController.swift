//
//  QQScanViewController.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/22/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import swiftScan

protocol QQScanViewControllerDelegate: class {
  func handleCodeResult(arrayResult: [LBXScanResult])
}

class QQScanViewController: LBXScanViewController {
  
  weak var resultDelegate: QQScanViewControllerDelegate?
  
  // MARK - Properties
  var isOpenedFlash: Bool = false

  var topTitle: UILabel?
  lazy var bottomItemsView: UIView = {
    let yMax = view.frame.maxY - view.frame.minY
    let bottomItemsView = UIView(frame: CGRect(x: 0.0, y: yMax-100, width: self.view.frame.size.width, height: 100 ) )
    bottomItemsView.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
    return bottomItemsView
  }()
  
  lazy var btnFlash: UIButton = {
    let size = CGSize(width: 65, height: 87)
    let btnFlash = UIButton()
    btnFlash.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    btnFlash.center = CGPoint(x: bottomItemsView.frame.width/2, y: bottomItemsView.frame.height/2)
    btnFlash.setImage(UIImage(named: "flash_nor"), for:UIControl.State.normal)
    btnFlash.addTarget(self, action: #selector(QQScanViewController.openOrCloseFlash), for: UIControl.Event.touchUpInside)
    return btnFlash
  }()
  
  // MARK: - View life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "Lector código QR"
    setNeedCodeImage(needCodeImg: true)
    scanStyle?.centerUpOffset += 10
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    drawBottomItems()
  }
  
  // MARK: - Private

  private func drawBottomItems() {
    bottomItemsView.addSubview(btnFlash)
    view.addSubview(bottomItemsView)
  }
  
  // MARK: - Public
  override func handleCodeResult(arrayResult: [LBXScanResult]) {
    // Hamdle array result
   resultDelegate?.handleCodeResult(arrayResult: arrayResult)
  }
  
  @objc func openOrCloseFlash() {
    scanObj?.changeTorch()
    isOpenedFlash = !isOpenedFlash
    if isOpenedFlash {
      btnFlash.setImage(UIImage(named: "flash"), for:UIControl.State.normal)
    } else {
      btnFlash.setImage(UIImage(named: "flash_nor"), for:UIControl.State.normal)
    }
  }
}
