//
//  DemoFaceCaptureSimpleController.swift
//  DOTSample
//
//  Created by Dominik Pethö on 8/15/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit
import DOT
import AVKit

protocol DemoVerificationControllerDelegate: class {
  func demoVerificationController(_ controller: DemoVerificationController, verified: Bool)
}

class DemoVerificationController: UIViewController {
  
    weak var delegate: DemoVerificationControllerDelegate?
    @IBOutlet weak var containerView: UIView!
    
    private var faceVerifier: FaceVerifier!
    private var verificationProcessIsCompleted = false
    
    let sharedTemplate = UserDefaults.standard.array(forKey: "template") as? [Int8]
    
    private lazy var faceCaptureSimpleController: FaceCaptureSimpleController = {
        let controller = FaceCaptureSimpleController.create(requestFullImage: true, requestCropImage: true, requestTemplate: true)
        controller.delegate = self
        return controller
    }()
    
    static func create() -> DemoVerificationController {
        let controller: UIViewController = instantiateFromStoryboard(storybaordName: "DemoVerificationController")
        return controller as! DemoVerificationController
    }
    
}

//MARK: - Lifecycle
extension DemoVerificationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addChild(faceCaptureSimpleController)
        faceCaptureSimpleController.view.translatesAutoresizingMaskIntoConstraints = true
        faceCaptureSimpleController.view.frame = containerView.bounds
        containerView.addSubview(faceCaptureSimpleController.view)
        faceCaptureSimpleController.didMove(toParent: self)
        
        faceVerifier = FaceVerifier()
        faceVerifier.delegate = self
        title = "Verificación de rostro"
    }
    
}

//MARK: - Fileprivates
extension DemoVerificationController {
    
    private func checkCameraPermission(status: AVAuthorizationStatus, controller: FaceCaptureSimpleController) {
        if status != .authorized {
            let alertController = UIAlertController(title: L10n.FaceCapture.Alert.Camera.title, message: L10n.FaceCapture.Alert.Camera.message, preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: L10n.FaceCapture.Alert.Camera.Option.settings, style: .default) { (_) -> Void in
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(settingsUrl)
            }
            
            let cancelAction = UIAlertAction(title: L10n.FaceCapture.Alert.Camera.Option.cancel, style: .cancel) { [weak self] (_) -> Void in
                self?.dismiss(animated: true)
            }
            
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            alertController.preferredAction = settingsAction
            
            controller.present(alertController, animated: true, completion: nil)
        } else {
            controller.requestFaceCapture()
        }
    }
    
}

extension DemoVerificationController: FaceCaptureSimpleControllerDelegate {
    
    func faceCaptureSimple(_ controller: FaceCaptureSimpleController, didCapture faceCaptureImage: FaceCaptureImage) {
        debugPrint(#function)
        guard let sharedTemplate = sharedTemplate else {
            controller.dismiss(animated: true, completion: nil)
            return
        }
        
        faceVerifier.verifyIfAvailable(probeImage: FaceImage.init(image: faceCaptureImage.fullImage!, minEyeDistanceRatio: 0.1, maxEyeDistanceRatio: 0.24), referenceTemplate: sharedTemplate, threshold: 50, checkMultipleFaces: true, templateId: 1)
    }
    
    func faceCaptureSimpleWillAppear(_ controller: FaceCaptureSimpleController) {
    }
    
    func faceCaptureSimpleDidAppear(_ controller: FaceCaptureSimpleController) {
        DispatchQueue.main.asyncAfter(seconds: 0.5) {
            controller.requestFaceCapture()
        }
    }
    
    func faceCaptureSimpleCameraInitFailed(_ controller: FaceCaptureSimpleController) {
        debugPrint(#function)
    }
    
    func faceCaptureSimpleNoCameraPermission(_ controller: FaceCaptureSimpleController) {
        debugPrint(#function)
        guard let status = DOT.authorizeCamera (onRequestAccess: { [weak self] in
            self?.checkCameraPermission(status: $0, controller: controller)
        }) else { return }
        
        checkCameraPermission(status: status, controller: controller)
    }
    
    func faceCaptureSimpleDidFailed(_ controller: FaceCaptureSimpleController) {
        debugPrint(#function)
        controller.stopFaceCapture()
    }
    
}

extension DemoVerificationController: FaceVerifierDelegate {
    
    func faceVerifier(_ faceVerifier: FaceVerifier, verifiedTemplate templateId: Int, score: Float) {
        debugPrint(#function)
        if !verificationProcessIsCompleted {
            faceCaptureSimpleController.stopFaceCapture()
            verificationProcessIsCompleted = true
           delegate?.demoVerificationController(self, verified: true)
        }
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, notVerifiedTemplate templateId: Int, score: Float) {
        debugPrint(#function)
        if !verificationProcessIsCompleted {
            verificationProcessIsCompleted = true
            faceCaptureSimpleController.stopFaceCapture()
          delegate?.demoVerificationController(self, verified: false)
        }
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, incompatibleTemplateId templateId: Int) {
        debugPrint(#function)
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, faceNotFoundOnProbe templateId: Int) {
        debugPrint(#function)
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, faceNotFoundOnReference templateId: Int) {
        debugPrint(#function)
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, multipleFaceFoundOnProbe count: Int, templateId: Int) {
        debugPrint(#function)
    }
    
    func faceVerifier(_ faceVerifier: FaceVerifier, multipleFaceFoundOnReference count: Int, templateId: Int) {
        debugPrint(#function)
    }
    
}
