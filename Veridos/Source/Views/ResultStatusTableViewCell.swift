//
//  ResultStatusTableViewCell.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/23/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

class ResultStatusTableViewCell: UITableViewCell {
  
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var statusColorView: UIView!
  
  
  override func awakeFromNib() {
    super.awakeFromNib()  
    statusColorView.layer.cornerRadius = statusColorView.frame.width / 2
  }
  
  func setup(item: ResultItem) {
    statusLabel.text = item.text
    statusColorView.backgroundColor = item.isOn ? .green : .red
  }
}
