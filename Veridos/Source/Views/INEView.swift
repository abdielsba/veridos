//
//  INEView.swift
//  Veridos
//
//  Created by Abdiel Soto on 1/2/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import UIKit

class INEView: NibView {

  @IBOutlet weak var imsgeView: UIImageView!
  @IBOutlet weak var lastNameLabel: UILabel!
  @IBOutlet weak var middleNameLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!

  @IBOutlet weak var address1Label: UILabel!
  @IBOutlet weak var address2Label: UILabel!
  @IBOutlet weak var address3Label: UILabel!
  
  @IBOutlet weak var electorKeyLabel: UILabel!
  @IBOutlet weak var curpLabel: UILabel!
  @IBOutlet weak var registerYear: UILabel!
  @IBOutlet weak var birthdayLabel: UILabel!
  @IBOutlet weak var genreLabel: UILabel!
  @IBOutlet weak var stateLabel: UILabel!
  @IBOutlet weak var municipalityLabel: UILabel!
  @IBOutlet weak var sectionLabel: UILabel!
  @IBOutlet weak var localityLabel: UILabel!
  @IBOutlet weak var emisionLabel: UILabel!
  

  
  func setIDInfo(_ IDInfo: IDInfo?) {
    
    guard let id = IDInfo else { return }
    
    lastNameLabel.text = id.lastName
    middleNameLabel.text = id.middleName
    nameLabel.text = id.name
    address1Label.text = id.address1
    address2Label.text = id.address2
    address3Label.text = id.address3
    electorKeyLabel.text = id.electorKey
    curpLabel.text = id.curp
    registerYear.text = id.registerYear
    birthdayLabel.text = id.birthDay
    genreLabel.text = id.gender
    stateLabel.text = id.state
    municipalityLabel.text = id.municipality
    sectionLabel.text = id.section
    localityLabel.text = id.locality
    emisionLabel.text = id.emisionYear
  }
}
