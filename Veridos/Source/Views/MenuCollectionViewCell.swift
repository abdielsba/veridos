//
//  MenuCollectionViewCell.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/23/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  
  
  func setup(item: MenuItem) {
    backgroundImageView.image = item.image
    setText(item.text)
  }
  
  private func setText(_ text: String) {
    
    // Create a shadow
    let myShadow = NSShadow()
    myShadow.shadowBlurRadius = 3
    myShadow.shadowOffset = CGSize(width: 4, height: 4)
    myShadow.shadowColor = UIColor.black
    
    // Create an attribute from the shadow
    let myAttribute = [NSAttributedString.Key.shadow: myShadow ]
    
    // Add the attribute to the string
    let myAttrString = NSAttributedString(string: text, attributes: myAttribute)
    titleLabel.attributedText = myAttrString
  }
}
