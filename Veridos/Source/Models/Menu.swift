//
//  Menu.swift
//  Veridos
//
//  Created by Abdiel Soto on 11/22/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit

enum MenuType {
  case none
  case visualizarIne
  case verificarIne
  case verificarOffline
  case miIne
  case validarExtranjero
}

struct MenuItem {
  
  let type: MenuType
  let text: String
  let image: UIImage?
}
