//
//  IDInfo.swift
//  Veridos
//
//  Created by Abdiel Soto on 12/31/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import Foundation

struct IDInfo {
    
  let lastName: String
  let middleName: String
  let name: String
  let electorKey: String
  let curp: String
  let birthDay: String
  let date: String
  let gender: String
  let emisionYear: String
  let registerYear: String
  let state: String
  let municipality: String
  let section: String
  let locality: String
  let unknownId1: String
  let unknownId2: String
  let address1: String
  let address2: String
  let address3: String
  let unknownId3: String
  let fingerNumber1: String
  let fingerData1: String
  let fingerNumber2: String
  let fingerData2: String
}
