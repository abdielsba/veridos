//
//  DOTManager.swift
//  Veridos
//
//  Created by Abdiel Soto on 1/24/19.
//  Copyright © 2019 MobTk. All rights reserved.
//

import DOT

class DOTManager {
  
  private init () { }
  
  class func initialize() {
    debugPrint("LicenseId: " + DOT.licenseId!)
    if let path = Bundle.main.path(forResource: "iengine", ofType: "lic") {
      do {
        let license = try License(path: path)
        DOT.initialize(with: license)
        
      } catch {
        print(error)
      }
    }
  }

  class func clearData() {
    UserDefaults.standard.set(nil, forKey: "template")
  }
}
