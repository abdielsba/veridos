//
//  antispoofTfWrap.h
//  Identy1
//
//  Created by Sumuga on 4/4/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//


#ifndef antispoofTfWrap_h
#define antispoofTfWrap_h


#endif
#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface antispoofTfWrap : NSObject
- (void) loadModel:(NSString*)graphFileName memMapped:(bool)map optEnv:(bool)opt;
- (void) setInputLayer:(NSString*)inLayer outputLayer:(NSString*)outLayer;
- (void) setInputMean:(float)mean std:(float)std;
- (NSArray*)runOnFrame:(CVPixelBufferRef)pixelBuffer;
//- (NSArray*) getLabels;
- (void) clean;
@end
