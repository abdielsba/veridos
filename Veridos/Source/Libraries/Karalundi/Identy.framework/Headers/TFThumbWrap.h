//
//  TFThumbWrap.h
//  Identy
//
//  Created by mac on 6/21/18.
//  Copyright © 2018 Identy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface TFThumbWrap : NSObject
- (void) loadModel:(NSString*)graphFileName memMapped:(bool)map optEnv:(bool)opt;
- (void) setInputLayer:(NSString*)inLayer outputLayer:(NSString*)outLayer;
- (void) setInputMean:(float)mean std:(float)std;
- (NSArray*)runOnFrame:(CVPixelBufferRef)pixelBuffer;
//- (NSArray*) getLabels;
- (void) clean;
@end

