//
//  CppWrapper.h
//  Identy1
//
//  Created by Sumuga on 4/4/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CppWrapper : NSObject
+ (UIImage *_Nonnull) cropImage:(nonnull UIImage *)input finger:(NSString *_Nonnull)name;
+ (UIImage *_Nonnull)GetBinaryImage:(nonnull UIImage *)inputCropped hand:(NSString *_Nonnull)hand finger:(NSString *_Nonnull)name  action:(NSString *_Nonnull)action ;
+ (CIImage *_Nonnull)getHsvImage:(nonnull CIImage *)image;

+ (UIImage *_Nonnull)getEllipse:(UIImage *_Nonnull)inputCropped;
+ (UIImage *_Nonnull)arrayToImg:(NSArray *_Nonnull)arr segImage:(UIImage*_Nonnull)img finger:(NSString *_Nonnull)name;
+ (BOOL)isStable:(UIImage*_Nonnull)img1 previousFrame:(UIImage*_Nonnull)img2 thres:(double)value;
+ (UIImage *_Nullable)boxResizedImage:(UIImage*_Nonnull)img1 width:(int)w height:(int)h;
+ (void)generateISOTemplate:(NSString*_Nonnull)wsqfilePath width:(int)w height:(int)h outputPath:(NSString*_Nonnull)outPath;
+ (void)generateISOTemplate2:(NSString*_Nonnull)xytPath minFilePath:(NSString*_Nonnull)minpath outputPath:(NSString*_Nonnull)outPath;
@end
