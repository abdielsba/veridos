//
//  TfWrap.h
//  Identy1
//
//  Created by Sumuga on 2/20/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//

#ifndef tfWrap_h
#define tfWrap_h


#endif /* tfWrap_h */

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface tfWrap : NSObject
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName;
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName memMapped:(bool)map;
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName memMapped:(bool)map optEnv:(bool)opt;
- (void) setInputLayer:(NSString*)inLayer outputLayer:(NSString*)outLayer;
- (void) setInputMean:(float)mean std:(float)std;
- (NSArray*)runOnFrame:(CVPixelBufferRef)pixelBuffer;
- (NSArray*) getLabels;
- (void) clean;
@end

