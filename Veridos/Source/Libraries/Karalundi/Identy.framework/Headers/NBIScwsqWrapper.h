//
//  NBIScwsqWrapper.h
//  Identy1
//
//  Created by Sumuga on 5/21/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NBIScwsqWrapper : NSObject
+ (NSUInteger)generateWsQ:(NSString *)imagePath deleteInputWhenDone:(BOOL)shouldDeleteInput binaryImage:(UIImage *)image birate:(float)bitrate;
@end
