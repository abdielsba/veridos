//
//  NBISWrapper.h
//  Identy1
//
//  Created by Sumuga on 5/18/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NBISWrapper : NSObject
+ (NSString *)extractTemplate:(NSString *)inputPath intoDirectory:(NSString *)outputPath deleteInputWhenDone:(BOOL)shouldDeleteInput ;
+ (NSUInteger)computeNFIQ:(NSString *)imagePath deleteInputWhenDone:(BOOL)shouldDeleteInput ;
@end
