//
//  Identy.h
//  Identy
//
//  Created by Sumuga on 6/11/18.
//  Copyright © 2018 Identy. All rights reserved.
//
#import <Foundation/Foundation.h>

#import "TfWrap.h"
#import "antispoofTfWrap.h"
#import "QualityCheck.h"
#import "NBIScwsqWrapper.h"
#import "CppWrapper.h"
#import "bozorthWrapper.h"
#import "NBISWrapper.h"
#import "NSData+AESCrypt.h"
#import "TFThumbWrap.h"
#import <UIKit/UIKit.h>

@class IdentyFramework;
//! Project version number for Identy.
FOUNDATION_EXPORT double IdentyVersionNumber;

//! Project version string for Identy.
FOUNDATION_EXPORT const unsigned char IdentyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Identy/PublicHeader.h>


