//
//  QualityCheck.h
//  Identy1
//
//  Created by Sumuga on 4/4/18.
//  Copyright © 2018 Sumuga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface QualityCheck : NSObject
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName;
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName memMapped:(bool)map;
- (void) loadModel:(NSString*)graphFileName labels:(NSString*)labelsFileName memMapped:(bool)map optEnv:(bool)opt;
- (void) setInputLayer:(NSString*)inLayer outputLayer:(NSString*)outLayer;
- (void) setInputMean:(float)mean std:(float)std;
- (NSArray*)runOnFrame:(CVPixelBufferRef)pixelBuffer;
- (NSArray*) getLabels;
- (void) clean;
@end
