//
//  IdentiManager.swift
//  Veridos
//
//  Created by Abdiel Soto Barrera on 11/29/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import Identy

enum Result<T, E: Error> {
  case success(T)
  case failure(E)
}

enum ErrorResult: Error {
  case custom(string: String)
}

extension ErrorResult: LocalizedError {
  public var errorDescription: String? {
    switch self {
    case .custom(let string):
      return NSLocalizedString(string, comment: "Invalid")
    }
  }
}

class IdentyManager {
  
  // MARK: - Indenty properties
  private var plainSolidColor:UIColor! = nil
  private var boxesColor:UIColor! = nil
  private var boxes_transparent:UIColor! = nil
  private var boxes_transparent_innerborder:UIColor! = nil
  private var startColor:UIColor! = nil
  private var middleColor:UIColor! = nil
  private var endColor:UIColor! = nil
  private var totalPreviewCount: Int = 0
  private var totalCaptureCount: Int = 0
  private var isLeftMissingFingerSelected: Bool! = false
  private var isRightMissingFingerSelected: Bool! = false
  private var leftMissingFingerCount = 0
  private var rightMissingFingerCount = 0
  private var rightMissingArray: [Int] = []
  private var leftMissingArray: [Int] = []
  private var gif4FData:Data! = nil
  private var gifThumbData:Data! = nil
  
  // MARK: - Manager properties
  private weak var controller: UIViewController?
  private let IDInfo: IDInfo?
  
  // MARK: - Constants
  struct Configuration {
    static let license = Bundle.main.path(forResource: "com.identity.demo", ofType: "lic")!
  }
  
  // MARK: - Initilization
  init(IDInfo:IDInfo?, viewController: UIViewController) {
    self.IDInfo = IDInfo
    self.controller = viewController
  }
  
  // MARK: - Public
  func enroll(completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    
    let instance = IdentyFramework.init(with: Configuration.license, localizablePath: Singleton.sharedInstance.languagePath(), table: "Main")
    // UI
    updateUI(identy: instance)
    
    guard let idInfo = IDInfo else {
      return completion(Result.failure(ErrorResult.custom(string: "Missing QR data")))
    }
    
    // Configuration
    instance.handScanTypeArray = getScanFingers(with: idInfo)
    instance.wsqCompression = .WSQ_10_1
    instance.isNeedShowTraining = true
    instance.isAntiSpoofCheck = true
    instance.isDemo = false
    instance.displayResult = true
    instance.isBoxes = true
    instance.uiSelect = .boxes
    
    instance.qualityCheckRegions = [.index, .middle, .little, .ring]
    instance.gif4FData = gif4FData
    instance.gifThumbData = gifThumbData
    instance.plainSolidColor = plainSolidColor
    instance.boxesColor = boxesColor
    instance.startColor = startColor
    instance.middleColor = middleColor
    instance.endColor = endColor
    instance.boxes_transparent = boxes_transparent
    instance.boxes_transparent_innerborder = boxes_transparent_innerborder
    instance.templates.removeAll()
    instance.templates.append(.wsq)
    instance.templates.append(.raw)
    guard let viewController =  controller else {
      completion(Result.failure(ErrorResult.custom(string: "Missing UIViewController")))
      return
    }
    instance.enroll(viewcontrol: viewController) { (success, responseModel, errorMessage) in
      guard let model = responseModel else {
        guard let message = errorMessage else {
          completion(Result.failure(ErrorResult.custom(string: "Unknown error")))
          return
        }
        completion(Result.failure(ErrorResult.custom(string: message)))
        return
      }
      completion(Result.success(model))
      self.saveEnrollFlag()
    }
  }
  
  func verify(completion: @escaping ((Result<IdentyResponse, ErrorResult>) -> Void)) {
    let instance = IdentyFramework.init(with: Configuration.license, localizablePath: Singleton.sharedInstance.languagePath(), table: "Main")
    
    guard let idInfo = IDInfo else {
      return completion(Result.failure(ErrorResult.custom(string: "Missing QR data")))
    }
    
    // UI
    updateUI(identy: instance)
    // Finger configuration
    instance.handScanTypeArray = getScanFingers(with: idInfo)
    instance.wsqCompression = .WSQ_10_1
    instance.isNeedShowTraining = true
    instance.isAntiSpoofCheck = true
    instance.isDemo = false
    instance.templates.removeAll()
    instance.templates.append(.wsq)
    instance.templates.append(.raw)
    instance.isBoxes = true
    instance.gif4FData = gif4FData
    instance.gifThumbData = gifThumbData
    instance.plainSolidColor = plainSolidColor
    instance.boxesColor = boxesColor
    instance.startColor = startColor
    instance.middleColor = middleColor
    instance.endColor = endColor
    instance.boxes_transparent = boxes_transparent
    instance.boxes_transparent_innerborder = boxes_transparent_innerborder
    guard let viewController =  controller else {
      completion(Result.failure(ErrorResult.custom(string: "Missing UIViewController")))
      return
    }
    instance.verify(viewcontrol: viewController) { (success, responseModel, errorMessage) in
      guard let model = responseModel else {
        guard let message = errorMessage else {
          completion(Result.failure(ErrorResult.custom(string: "Unknown error")))
          return
        }
        completion(Result.failure(ErrorResult.custom(string: message)))
        return
      }
      
      guard let responseDictionary = model.responseDictionary,
      let verifyResult = responseDictionary["verify_result"] as? [String: Bool] else {
          completion(Result.failure(ErrorResult.custom(string: "Verificación fallida, vuelva a intentarlo")))
          return
      }
      
      for fingerId in verifyResult.keys {
        let validationResult = verifyResult[fingerId]
        if let isValid = validationResult, isValid == false  {
          completion(Result.failure(ErrorResult.custom(string: "Verificación no satisfactoria, por favor vuelva a intentarlo")))
          return
        }
      }
      completion(Result.success(model))
    }
  }
  
  class func verify(model: IdentyResponse, IDInfo: IDInfo) {
    
    guard let responseDictionary = model.responseDictionary,
      let data = responseDictionary["data"] as? [String: Any] else {
        return
    }
    
    var fingerRightName = ""
    var fingerLeftName = ""

    switch IDInfo.fingerNumber1 {
    case "1":
      fingerRightName = "rigthlittle"
    case "2":
      fingerRightName = "rigthring"
    case "3":
      fingerRightName = "rigthmiddle"
    case "4":
      fingerRightName = "rigthindex"
    case "5":
      fingerRightName = "rigththumb"
    default:
      break
    }
    
    switch IDInfo.fingerNumber2 {
    case "6":
      fingerLeftName = "leftthumb"
    case "7":
      fingerLeftName = "leftindex"
    case "8":
      fingerLeftName = "leftmiddle"
    case "9":
      fingerLeftName = "leftring"
    case "10":
      fingerLeftName = "leftlittle"
    default:
      break
    }
    
    if let rigthFinger = data[fingerRightName] as? [String: Any],
      let templates = rigthFinger["templates"] as? [String: Any],
      let wsqData = templates["wsq"] as? String  {
      
      
    }
    
    if let leftFinger = data[fingerLeftName] as? [String: Any],
      let templates = leftFinger["templates"] as? [String: Any],
      let wsqData = templates["wsq"] as? String  {
    }
  }
  
  class func isEnrolled() -> Bool {
    return UserDefaults.standard.bool(forKey: "enroll")
  }
  
  class func clearData() {
    UserDefaults.standard.set(false, forKey: "enroll")
  }
  
  // MARK: - Private methods

  private func updateUI(identy: IdentyFramework) {
    identy.startColor = Theme.mainBackgroundColor
    identy.endColor =  Theme.mainBackgroundColor
    identy.boxesColor =  Theme.mainBackgroundColor
    identy.middleColor =  Theme.mainBackgroundColor
    identy.plainSolidColor = Theme.mainBackgroundColor
    identy.introScreenMiddleColor = Theme.mainBackgroundColor
    identy.introScreenStartColor = Theme.mainBackgroundColor
    identy.introScreenEndColor = Theme.mainBackgroundColor
    identy.scanningBarColor = Theme.mainBackgroundColor
    identy.boxes_transparent = Theme.mainBackgroundColor
    identy.boxes_transparent_innerborder = Theme.mainBackgroundColor
  }
  
  private func getScanFingers(with IDInfo: IDInfo) -> [HandScanType] {
   
    var fingers: [HandScanType] = []
    // Right hand
    switch IDInfo.fingerNumber1 {
    case "1":
      fingers.append(.rightThumb)
    case "2":
      fingers.append(.rightIndex)
    case "3":
      fingers.append(.rightMiddle)
    case "4":
      fingers.append(.rightRing)
    case "5":
      fingers.append(.rightLittle)
    default:
      break
    }
    
    switch IDInfo.fingerNumber2 {
    case "6":
      fingers.append(.leftThumb)
    case "7":
      fingers.append(.leftIndex)
    case "8":
      fingers.append(.leftMiddle)
    case "9":
      fingers.append(.leftRing)
    case "10":
      fingers.append(.leftLittle)
    default:
      break
    }
    return fingers
  }
  
  private func saveEnrollFlag() {
    UserDefaults.standard.set(true, forKey: "enroll")
  }
}
