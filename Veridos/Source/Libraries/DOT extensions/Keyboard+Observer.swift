//
//  Keyboard+Observer.swift
//  DOT
//
//  Created by Dominik Pethö on 9/4/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit

@objc protocol KeyboardObservable: class {
    @objc optional func registerKeyboard()
    
    @objc optional func unregisterKeyboard()
    
    @objc optional func keyboardShown(notification: NSNotification)
    
    func keyboardShown(keyboardFrame: CGRect)
    
    func keyboardHidden()
}

extension KeyboardObservable where Self: UIViewController {
    
    func registerKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(Self.self.keyboardShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Self.self.keyboardHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unregisterKeyboard() {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension UIViewController: KeyboardObservable {
    
    @objc func keyboardShown(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                keyboardShown(keyboardFrame: keyboardFrame)
            }
        }
    }
    
    func keyboardShown(keyboardFrame: CGRect) {
        
    }
    
    func keyboardHidden() {
        
    }
}
