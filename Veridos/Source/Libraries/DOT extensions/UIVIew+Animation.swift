//
//  UIVIew+Animation.swift
//  DOT
//
//  Created by Dominik Pethö on 8/16/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit

public extension UIView {
    
    func push(scale: CGFloat = 0.95 , completion:@escaping ((Bool) -> Void)) {
        UIView.animate(withDuration: 0.05, animations: {
            self.transform = CGAffineTransform(scaleX: scale, y: scale) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.transform = CGAffineTransform.identity
                }) { finish in
                    completion(finish)
                }
        })
    }
    
    public func blink(enabled: Bool = true, duration: CFTimeInterval = 0.3) {
        enabled ? (UIView.animate(withDuration: duration, //Time duration you want,
            delay: 0.0,
            options: [],
            animations: { [weak self] in self?.alpha = 1.0 },
            completion: { [weak self] _ in self?.alpha = 0.0 })) : self.layer.removeAllAnimations()
    }
}
