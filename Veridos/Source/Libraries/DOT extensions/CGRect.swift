//
//  CGRect.swift
//  DOT
//
//  Created by Dominik Pethö on 9/3/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    
    func normalized() -> CGRect {
        
        return CGRect.init(x: origin.x, y: origin.y, width: width - origin.x, height: height - origin.y)
    }
    
}
