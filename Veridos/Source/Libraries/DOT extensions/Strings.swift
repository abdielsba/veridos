// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal enum L10n {
    
    internal enum FaceCapture {
        /// BUBU That's it!
        internal static let done = L10n.tr("Localizable", "face_capture.done")
        /// BUBU Enrollment failed
        internal static let failed = L10n.tr("Localizable", "face_capture.failed")
        /// BUBU Please read the following tips:
        internal static let failedTip = L10n.tr("Localizable", "face_capture.failedTip")
        /// BUBU Enrollment
        internal static let failedTitle = L10n.tr("Localizable", "face_capture.failedTitle")
        /// BUBU Enrollment
        internal static let title = L10n.tr("Localizable", "face_capture.title")
        
        internal enum Alert {
            
            internal enum Camera {
                /// BUBU Go to Settings?
                internal static let message = L10n.tr("Localizable", "face_capture.alert.camera.message")
                /// BUBU Camera usage not allowed
                internal static let title = L10n.tr("Localizable", "face_capture.alert.camera.title")
                
                internal enum Option {
                    /// BUBU Cancel
                    internal static let cancel = L10n.tr("Localizable", "face_capture.alert.camera.option.cancel")
                    /// BUBU Settings
                    internal static let settings = L10n.tr("Localizable", "face_capture.alert.camera.option.settings")
                }
            }
        }
        
        internal enum Confirmation {
            /// BUBU Accept
            internal static let accept = L10n.tr("Localizable", "face_capture.confirmation.accept")
            /// BUBU Verify resulting photo
            internal static let title = L10n.tr("Localizable", "face_capture.confirmation.title")
            /// BUBU Try again
            internal static let tryAgain = L10n.tr("Localizable", "face_capture.confirmation.tryAgain")
        }
        
        internal enum Hint {
            /// BUBU Center your face into the circle
            internal static let centerFace = L10n.tr("Localizable", "face_capture.hint.centerFace")
            /// BUBU That's it!
            internal static let done = L10n.tr("Localizable", "face_capture.hint.done")
            /// BUBU Rotate face towards light source
            internal static let light = L10n.tr("Localizable", "face_capture.hint.light")
            /// BUBU Start
            internal static let start = L10n.tr("Localizable", "face_capture.hint.start")
            /// BUBU Instructions
            internal static let title = L10n.tr("Localizable", "face_capture.hint.title")
        }
        
        internal enum InstructionStep {
            /// BUBU Stay still!
            internal static let capture = L10n.tr("Localizable", "face_capture.instruction_step.capture")
            
            internal enum Center {
                /// BUBU Center your face
                internal static let first = L10n.tr("Localizable", "face_capture.instruction_step.center.first")
                /// BUBU Move your face to the center of the circle
                internal static let second = L10n.tr("Localizable", "face_capture.instruction_step.center.second")
            }
            
            internal enum Close {
                /// BUBU Move back
                internal static let first = L10n.tr("Localizable", "face_capture.instruction_step.close.first")
                /// BUBU Move your device away from your face
                internal static let second = L10n.tr("Localizable", "face_capture.instruction_step.close.second")
            }
            
            internal enum Far {
                /// BUBU Move closer
                internal static let first = L10n.tr("Localizable", "face_capture.instruction_step.far.first")
                /// BUBU Move your device closer to your face
                internal static let second = L10n.tr("Localizable", "face_capture.instruction_step.far.second")
            }
            
            internal enum Light {
                /// BUBU Find a front light source.\nAvoid lights casting strong shadows on your face (top or side lights).\nAvoid strong back-light.
                internal static let andvancedText = L10n.tr("Localizable", "face_capture.instruction_step.light.andvancedText")
                /// BUBU Rotate towards light
                internal static let andvancedTitle = L10n.tr("Localizable", "face_capture.instruction_step.light.andvancedTitle")
                /// BUBU Move towards light
                internal static let first = L10n.tr("Localizable", "face_capture.instruction_step.light.first")
                /// BUBU Rotate your face towards the light
                internal static let second = L10n.tr("Localizable", "face_capture.instruction_step.light.second")
                /// BUBU Rotate face towards light source
                internal static let simple = L10n.tr("Localizable", "face_capture.instruction_step.light.simple")
            }
            
            internal enum Position {
                /// BUBU Hold the device at eye level.\n Align your face to the center of the circle.\nAdjust the distance between your device and your face.
                internal static let andvancedText = L10n.tr("Localizable", "face_capture.instruction_step.position.andvancedText")
                /// BUBU Center your face
                internal static let andvancedTitle = L10n.tr("Localizable", "face_capture.instruction_step.position.andvancedTitle")
                /// BUBU Center your face into the circle
                internal static let simple = L10n.tr("Localizable", "face_capture.instruction_step.position.simple")
            }
        }             
    }
    
    internal enum Liveness {
        /// BUBU Liveness Detection
        internal static let title = L10n.tr("Localizable", "liveness.title")
        /// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
        internal static let watchObject = L10n.tr("Localizable", "liveness.watchObject")
        
        internal enum InstructionStep {
            /// BUBU Hold the phone at the eye level
            internal static let eyePosition = L10n.tr("Localizable", "liveness.instruction_step.eyePosition")
            /// BUBU Remove your glasses
            internal static let glasses = L10n.tr("Localizable", "liveness.instruction_step.glasses")
            /// BUBU Rotate face towards light source
            internal static let light = L10n.tr("Localizable", "liveness.instruction_step.light")
            /// BUBU START
            internal static let start = L10n.tr("Localizable", "liveness.instruction_step.start")
        }
        
        internal enum State {
            /// BUBU Move towards light
            internal static let lowQuality = L10n.tr("Localizable", "liveness.state.lowQuality")
            /// BUBU
            internal static let multiFace = L10n.tr("Localizable", "liveness.state.multiFace")
            /// BUBU Look straight
            internal static let noFace = L10n.tr("Localizable", "liveness.state.noFace")
            /// BUBU Prepare for liveness detection
            internal static let ok = L10n.tr("Localizable", "liveness.state.ok")
            /// BUBU
            internal static let templateNotMatching = L10n.tr("Localizable", "liveness.state.templateNotMatching")
            /// BUBU Move back
            internal static let tooClose = L10n.tr("Localizable", "liveness.state.tooClose")
            /// BUBU Move closer
            internal static let tooFar = L10n.tr("Localizable", "liveness.state.tooFar")
        }
        
    }
    
    internal enum DocumentCapture {
        
        internal enum TypeBack {
            /// Take picture of back side
            internal static let hint = L10n.tr("Localizable", "document_capture.type_back.hint")
            /// Back Side
            internal static let side = L10n.tr("Localizable", "document_capture.type_back.side")
        }
        
        internal enum TypeFront {
            /// Take picture of front side
            internal static let hint = L10n.tr("Localizable", "document_capture.type_front.hint")
            /// Front Side
            internal static let side = L10n.tr("Localizable", "document_capture.type_front.side")
        }
    }
    
    internal enum DocumentReview {
        /// All correct
        internal static let allCorrenct = L10n.tr("Localizable", "document_review.allCorrenct")
        
        internal enum InfoBar {
            /// All items recognized
            internal static let ok = L10n.tr("Localizable", "document_review.info_bar.ok")
            /// %d
            internal static func uncertainItems(_ p1: Int) -> String {
                return L10n.tr("Localizable", "document_review.info_bar.uncertainItems", p1)
            }
        }
        
        internal enum Subtitle {
            /// All items were recognized successfully.
            internal static let ok = L10n.tr("Localizable", "document_review.subtitle.ok")
            /// Uncertain items require special attention.
            internal static let uncertain = L10n.tr("Localizable", "document_review.subtitle.uncertain")
        }
        
        internal enum Title {
            /// Please verify all the data.
            internal static let ok = L10n.tr("Localizable", "document_review.title.ok")
            /// Please verify all the data.
            internal static let uncertain = L10n.tr("Localizable", "document_review.title.uncertain")
        }
    }

}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
    fileprivate static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
