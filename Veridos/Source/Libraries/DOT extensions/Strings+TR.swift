//
//  Strings.swift
//  DOTSample
//
//  Created by Dominik Pethö on 11/21/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit

extension String {
    
    func frameSize(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size
    }
    
}
