//
//  Encodable.swift
//  DOTSample
//
//  Created by Dominik Pethö on 9/13/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import Foundation

extension Encodable {
    var jsonDictionary: [String: Any]? {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970 
        guard let data = try? encoder.encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
