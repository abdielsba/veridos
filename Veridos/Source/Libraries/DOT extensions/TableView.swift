//
//  TableView.swift
//  DOT
//
//  Created by Dominik Pethö on 9/2/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit

extension UITableView {
    
    /// Register reusable cell with specified class type.
    func registerCell<T: UITableViewCell>(fromClass type: T.Type, forCellReuseIdentifier: String? = nil)  {
        register(UINib(nibName: String(describing: type), bundle: nil), forCellReuseIdentifier: forCellReuseIdentifier ?? String(describing: type))
    }
    
    
    /// Register reusable header footer view with specified class type.
    func registerHeaderFooterView<T: UITableViewHeaderFooterView>(fromClass type: T.Type) {
        register(UINib(nibName: String(describing: type), bundle: nil), forHeaderFooterViewReuseIdentifier: String(describing: type))
    }
    
    /// Dequeue reusable header footer view with specified class type.
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(fromClass type: T.Type) -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: String(describing: type)) as? T
    }
    
    /// Dequeue reusable cell with specified class type.
    func dequeueReusableCell<T: UITableViewCell>(fromClass type: T.Type, for indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withIdentifier: String(describing: type), for: indexPath) as? T
    }
    
    /// Dequeue reusable cell with specified class type.
    func dequeueReusableCell<T: UITableViewCell>(fromClass type: T.Type) -> T? {
        return dequeueReusableCell(withIdentifier: String(describing: type)) as? T
    }
    
}
