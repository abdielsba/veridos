//
//  ViewModel.swift
//  DOTSample
//
//  Created by Dominik Pethö on 9/17/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit

class ViewModel {
    
    lazy var actionQueue: DispatchQueue = {
        return DispatchQueue(label: "\(String(describing: ViewModel.self)).queue.network", qos: DispatchQoS.utility)
    }()    
    
   
    
}
