//
//  Collection.swift
//  DOTSample
//
//  Created by Dominik Pethö on 9/13/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import Foundation

extension Collection {
    func toDictionary<K, V>
        (_ transform:(_ element: Self.Iterator.Element) -> (key: K, value: V)?) -> [K : V] {
        var dictionary = [K : V]()
        for e in self {
            if let (key, value) = transform(e) {
                dictionary[key] = value
            }
        }
        return dictionary
    }
}
