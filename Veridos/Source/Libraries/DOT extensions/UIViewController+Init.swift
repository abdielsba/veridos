//
//  UIViewController+Create.swift
//  DOTSample
//
//  Created by Dominik Pethö on 8/13/18.
//  Copyright © 2018 Dominik Pethö. All rights reserved.
//

import UIKit
import AVFoundation

protocol WithProgressController: class {
    var progressController: UIAlertController? { get set }
    var navigationController: UINavigationController? { get }
    
    func showProgress()
    func hideProgress()
}

extension WithProgressController {
    
    func showProgress() {
        progressController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        guard let progressController = progressController else { return }
        
        let spinnerIndicator = UIActivityIndicatorView(style: .whiteLarge)
        
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        
        progressController.view.addSubview(spinnerIndicator)
        navigationController?.topViewController?.present(progressController, animated: false, completion: nil)
    }
    
    func hideProgress() {
        progressController?.dismiss(animated: true)
    }
    
}

extension UIViewController {
    
    static func instantiate<T: UIViewController>(fromController: T.Type) -> T {
        let bundle = Bundle.init(for: self)
        guard let controller = UIStoryboard(name: String(describing: T.self), bundle: bundle).instantiateInitialViewController() as? T else {
            fatalError("Storyboard wtih name \(String(describing: T.self)) not found and cannot be initialized")
        }
        
        return controller
    }
    
    static func instantiateFromStoryboard<T: UIViewController>(storybaordName: String) -> T {
        let bundle = Bundle.init(for: self)
        guard let controller = UIStoryboard(name: storybaordName, bundle: bundle).instantiateInitialViewController() as? T else {
            fatalError("Storyboard wtih name \(String(describing: T.self)) not found and cannot be initialized")
        }
        
        return controller
    }
    
}

protocol WithCameraChecker {
    func checkCameraPermission(condition: Bool)
    func cameraPermissionGranted()
    func noCameraPermission()
}

extension WithCameraChecker where Self: UIViewController {
    
    func checkCameraPermission(condition: Bool) {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        checkCameraAuthStatus(status, condition: condition)
    }
    
    private func checkCameraAuthStatus(_ status: AVAuthorizationStatus?, condition: Bool) {
        if let authorizationStatus = status, condition {
            if authorizationStatus == .authorized {
                cameraPermissionGranted()
            } else {
                noCameraPermission()
            }
        }
    }
}
