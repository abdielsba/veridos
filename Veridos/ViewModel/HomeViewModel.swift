//
//  HomeViewModel.swift
//  Veridos
//
//  Created by Abdiel Soto on 12/31/18.
//  Copyright © 2018 MobTk. All rights reserved.
//

import UIKit
import swiftScan
import Identy

class HomeViewModel {
  
  // MARK: - Public properties
  private(set) var IDInfo: IDInfo?
  
  // MARK: - Private properties
  private var listener: Listener?
  private var state: State = .idle {
    didSet {
      listener?(self.state)
    }
  }
  
  // MARK: - Public
  func getQRInfo(_ arrayResult: [LBXScanResult]) {
    
    guard let info = arrayResult.first,
          let stringScanned = info.strScanned,
          let idInfo = stringScanned.IDData() else {
        state = .failure()
      return
    }
    IDInfo = idInfo
    state = .successID(idInfo)
  }
  
  func verifyFingerPrint(_ response: IdentyResponse){
    
    guard let idInfo = IDInfo,
    let responseDictionary = response.responseDictionary else {
      state = .failure()
      return
    }
    
    
    

  }

}

extension HomeViewModel {
  
  enum State {
    case successID(IDInfo)
    case failure()
    case idle
  }
  
  typealias Listener = (State) -> ()
  
  func bindState(_ listener: @escaping Listener) {
    self.listener = listener
  }
  
  func unBindState() {
    listener = nil
  }
  
}
